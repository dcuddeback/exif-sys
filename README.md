# Libexif Rust Bindings

The `exif-sys` crate provides declarations and linkage for the `libexif` C library. Following the
`*-sys` package conventions, the `exif-sys` crate does not define higher-level abstractions over the
native `libexif` library functions.

## Dependencies

In order to use the `exif-sys` crate, you must have the `libexif` library installed where it can be
found by `pkg-config`.

## Usage
Add `exif-sys` as a dependency in `Cargo.toml`:

```toml
[dependencies]
exif-sys = "0.1"
```

Import the `exif_sys` crate and use the functions as they're defined in the native `libexif`
library. See the [`libexif` API documention](http://libexif.sourceforge.net/api/) for more usage
information.

## Finding Help

Since `exif-sys` is no more than a wrapper around the native `libexif` library, the best source for
help is the information already available for `libexif`:

* [Home Page](http://libexif.sourceforge.net/)
* [API Documentation](http://libexif.sourceforge.net/api/)

## License

Copyright © 2016 David Cuddeback

Distributed under the [MIT License](LICENSE).

*Note:* By using this crate, your executable will link to the `libexif` C library, which is licensed
under the [LGPL version 2.1](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html#TOC1).
