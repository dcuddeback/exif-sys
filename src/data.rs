use libc::{c_void, c_char, c_uchar, c_uint};

use byte_order::ExifByteOrder;
use content::ExifContent;
use data_type::ExifDataType;
use ifd::EXIF_IFD_COUNT;
use mem::ExifMem;
use mnote::ExifMnoteData;


pub use ExifDataOption::*;

#[repr(C)]
pub struct ExifData {
    pub ifd: [*mut ExifContent; EXIF_IFD_COUNT],
    data: *mut c_uchar,
    size: c_uint,
    private: *mut ExifDataPrivate,
}

#[repr(C)]
pub struct ExifDataPrivate {
    __private: c_void,
}

#[repr(C)]
pub enum ExifDataOption {
    EXIF_DATA_OPTION_IGNORE_UNKNOWN_TAGS    = 1 << 0,
    EXIF_DATA_OPTION_FOLLOW_SPECIFICATION   = 1 << 1,
    EXIF_DATA_OPTION_DONT_CHANGE_MAKER_NOTE = 1 << 2,
}

pub type ExifDataForeachContentFunc = extern "C" fn (content: *mut ExifContent, data: *mut c_void);

extern "C" {
    pub fn exif_data_new() -> *mut ExifData;
    pub fn exif_data_new_mem(mem: *mut ExifMem) -> *mut ExifData;
    pub fn exif_data_new_from_file(path: *mut c_char) -> *mut ExifData;
    pub fn exif_data_new_from_data(data: *mut c_uchar, size: c_uint) -> *mut ExifData;
    pub fn exif_data_load_data(data: *mut ExifData, buf: *const c_uchar, size: c_uint);
    pub fn exif_data_save_data(data: *mut ExifData, buf: *mut *const c_uchar, ds: c_uint);
    pub fn exif_data_ref(data: *mut ExifData);
    pub fn exif_data_unref(data: *mut ExifData);
    pub fn exif_data_free(data: *mut ExifData);
    pub fn exif_data_get_byte_order(data: *mut ExifData) -> ExifByteOrder;
    pub fn exif_data_set_byte_order(data: *mut ExifData, order: ExifByteOrder);
    pub fn exif_data_get_mnote_data(data: *mut ExifData) -> *mut ExifMnoteData;
    pub fn exif_data_fix(data: *mut ExifData);
    pub fn exif_data_foreach_content(data: *mut ExifData, func: ExifDataForeachContentFunc, data: *mut c_void);
    pub fn exif_data_option_get_name(option: ExifDataOption) -> *const c_char;
    pub fn exif_data_option_get_description(option: ExifDataOption) -> *const c_char;
    pub fn exif_data_set_option(data: *mut ExifData, option: ExifDataOption);
    pub fn exif_data_unset_option(data: *mut ExifData, option: ExifDataOption);
    pub fn exif_data_set_data_type(data: *mut ExifData, dt: ExifDataType);
    pub fn exif_data_get_data_type(data: *mut ExifData) -> ExifDataType;
    pub fn exif_data_dump(data: *mut ExifData);
}
