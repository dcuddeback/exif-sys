use libc::{c_void, c_char, c_uchar, c_uint};

#[repr(C)]
pub struct ExifMnoteData {
    __private: c_void,
}

extern "C" {
    pub fn exif_mnote_data_ref(mnote: *mut ExifMnoteData);
    pub fn exif_mnote_data_unref(mnote: *mut ExifMnoteData);
    pub fn exif_mnote_data_load(mnote: *mut ExifMnoteData, buf: *const c_uchar, size: c_uint);
    pub fn exif_mnote_data_save(mnote: *mut ExifMnoteData, buf: *mut *const c_uchar, size: *mut c_uint);
    pub fn exif_mnote_data_count(mnote: *mut ExifMnoteData) -> c_uint;
    pub fn exif_mnote_data_get_id(mnote: *mut ExifMnoteData, n: c_uint) -> c_uint;
    pub fn exif_mnote_data_get_name(mnote: *mut ExifMnoteData, n: c_uint) -> *const c_char;
    pub fn exif_mnote_data_get_title(mnote: *mut ExifMnoteData, n: c_uint) -> *const c_char;
    pub fn exif_mnote_data_get_description(mnote: *mut ExifMnoteData, n: c_uint) -> *const c_char;
    pub fn exif_mnote_data_get_value(mnote: *mut ExifMnoteData, n: c_uint, val: *mut c_char, maxlen: c_uint) -> *mut c_char;
}
