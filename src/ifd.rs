use libc::c_char;

pub use ExifIfd::*;

#[derive(Debug,PartialEq,Eq,Clone,Copy,Hash)]
#[repr(C)]
pub enum ExifIfd {
    EXIF_IFD_0 = 0,
    EXIF_IFD_1,
    EXIF_IFD_EXIF,
    EXIF_IFD_GPS,
    EXIF_IFD_INTEROPERABILITY,
    EXIF_IFD_UNKNOWN,
}

pub const EXIF_IFD_COUNT: usize = 5;

extern "C" {
    pub fn exif_ifd_get_name(ifd: ExifIfd) -> *const c_char;
}
