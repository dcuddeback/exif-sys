pub use ExifDataType::*;

#[derive(Debug,PartialEq,Eq,Clone,Copy,Hash)]
#[repr(C)]
pub enum ExifDataType {
    EXIF_DATA_TYPE_UNCOMPRESSED_CHUNKY = 0,
    EXIF_DATA_TYPE_UNCOMPRESSED_PLANAR,
    EXIF_DATA_TYPE_UNCOMPRESSED_YCC,
    EXIF_DATA_TYPE_COMPRESSED,
    EXIF_DATA_TYPE_UNKNOWN,
}

pub const EXIF_DATA_TYPE_COUNT: usize = 4;
