use libc::c_void;

use utils::ExifLong;

pub type ExifMemAllocFunc = extern "C" fn (s: ExifLong) -> *mut c_void;
pub type ExifMemReallocFunc = extern "C" fn (p: *mut c_void, s: ExifLong) -> *mut c_void;
pub type ExifMemFreeFunc = extern "C" fn (p: *mut c_void);

#[repr(C)]
pub struct ExifMem {
    __private: c_void,
}

extern "C" {
    pub fn exif_mem_new(a: ExifMemAllocFunc, r: ExifMemReallocFunc, f: ExifMemFreeFunc) -> *mut ExifMem;
    pub fn exif_mem_ref(m: *mut ExifMem);
    pub fn exif_mem_unref(m: *mut ExifMem);
    pub fn exif_mem_alloc(m: *mut ExifMem, s: ExifLong) -> *mut c_void;
    pub fn exif_mem_realloc(m: *mut ExifMem, p: *mut c_void, s: ExifLong) -> *mut c_void;
    pub fn exif_mem_free(m: *mut ExifMem, p: *mut c_void);
    pub fn exif_mem_new_default() -> *mut ExifMem;
}
