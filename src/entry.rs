use libc::{c_void, c_char, c_uchar, c_uint, c_ulong};

use content::ExifContent;
use format::ExifFormat;
use mem::ExifMem;
use tag::ExifTag;

#[repr(C)]
pub struct ExifEntry {
    pub tag: ExifTag,
    pub format: ExifFormat,
    pub components: c_ulong,
    pub data: *mut c_uchar,
    pub size: c_uint,
    pub parent: *mut ExifContent,
    private: *mut ExifEntryPrivate,
}

#[repr(C)]
struct ExifEntryPrivate {
    __private: c_void,
}

extern "C" {
    pub fn exif_entry_new() -> *mut ExifEntry;
    pub fn exif_entry_new_mem(mem: *mut ExifMem) -> *mut ExifEntry;
    pub fn exif_entry_ref(entry: *mut ExifEntry);
    pub fn exif_entry_unref(entry: *mut ExifEntry);
    pub fn exif_entry_free(entry: *mut ExifEntry);
    pub fn exif_entry_initialize(entry: *mut ExifEntry, tag: ExifTag);
    pub fn exif_entry_fix(entry: *mut ExifEntry);
    pub fn exif_entry_get_value(entry: *mut ExifEntry, val: *mut c_char, maxlen: c_uint) -> *const c_char;
    pub fn exif_entry_dump(entry: *mut ExifEntry, indent: c_uint);
}
