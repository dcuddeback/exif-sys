use libc::{c_char, c_uchar, c_schar, c_ushort, c_int};

use byte_order::ExifByteOrder;

pub type ExifByte = c_uchar;
pub type ExifSByte = c_schar;
pub type ExifAscii = *mut c_char;
pub type ExifShort = u16;
pub type ExifSShort = i16;
pub type ExifLong = u32;
pub type ExifSLong = i32;
pub type ExifUndefined = c_char;

#[repr(C)]
pub struct ExifRational {
    pub numerator: ExifLong,
    pub denominator: ExifLong,
}

#[repr(C)]
pub struct ExifSRational {
    pub numerator: ExifSLong,
    pub denominator: ExifSLong,
}

extern "C" {
    pub fn exif_byte_order_get_name(order: ExifByteOrder) -> *const c_char;

    pub fn exif_get_short(b: *const c_uchar, order: ExifByteOrder) -> ExifShort;
    pub fn exif_get_sshort(b: *const c_uchar, order: ExifByteOrder) -> ExifSShort;
    pub fn exif_get_long(b: *const c_uchar, order: ExifByteOrder) -> ExifLong;
    pub fn exif_get_slong(b: *const c_uchar, order: ExifByteOrder) -> ExifSLong;
    pub fn exif_get_rational(b: *const c_uchar, order: ExifByteOrder) -> ExifRational;
    pub fn exif_get_srational(b: *const c_uchar, order: ExifByteOrder) -> ExifSRational;

    pub fn exif_set_short(b: *mut c_uchar, order: ExifByteOrder, value: ExifShort);
    pub fn exif_set_sshort(b: *mut c_uchar, order: ExifByteOrder, value: ExifSShort);
    pub fn exif_set_long(b: *mut c_uchar, order: ExifByteOrder, value: ExifLong);
    pub fn exif_set_slong(b: *mut c_uchar, order: ExifByteOrder, value: ExifSLong);
    pub fn exif_set_rational(b: *mut c_uchar, order: ExifByteOrder, value: ExifRational);
    pub fn exif_set_srational(b: *mut c_uchar, order: ExifByteOrder, value: ExifSRational);

    pub fn exif_convert_utf16_to_utf8(output: *mut c_char, input: *const c_ushort, maxlen: c_int);
}
