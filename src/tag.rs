use libc::{c_char, c_uint};

use ifd::ExifIfd;
use data_type::ExifDataType;

pub use ExifTag::*;
pub use ExifSupportLevel::*;

pub const EXIF_TAG_GPS_VERSION_ID:         u16 = 0x0000;
pub const EXIF_TAG_GPS_LATITUDE_REF:       u16 = 0x0001;
pub const EXIF_TAG_GPS_LATITUDE:           u16 = 0x0002;
pub const EXIF_TAG_GPS_LONGITUDE_REF:      u16 = 0x0003;
pub const EXIF_TAG_GPS_LONGITUDE:          u16 = 0x0004;
pub const EXIF_TAG_GPS_ALTITUDE_REF:       u16 = 0x0005;
pub const EXIF_TAG_GPS_ALTITUDE:           u16 = 0x0006;
pub const EXIF_TAG_GPS_TIME_STAMP:         u16 = 0x0007;
pub const EXIF_TAG_GPS_SATELLITES:         u16 = 0x0008;
pub const EXIF_TAG_GPS_STATUS:             u16 = 0x0009;
pub const EXIF_TAG_GPS_MEASURE_MODE:       u16 = 0x000A;
pub const EXIF_TAG_GPS_DOP:                u16 = 0x000B;
pub const EXIF_TAG_GPS_SPEED_REF:          u16 = 0x000C;
pub const EXIF_TAG_GPS_SPEED:              u16 = 0x000D;
pub const EXIF_TAG_GPS_TRACK_REF:          u16 = 0x000E;
pub const EXIF_TAG_GPS_TRACK:              u16 = 0x000F;
pub const EXIF_TAG_GPS_IMG_DIRECTION_REF:  u16 = 0x0010;
pub const EXIF_TAG_GPS_IMG_DIRECTION:      u16 = 0x0011;
pub const EXIF_TAG_GPS_MAP_DATUM:          u16 = 0x0012;
pub const EXIF_TAG_GPS_DEST_LATITUDE_REF:  u16 = 0x0013;
pub const EXIF_TAG_GPS_DEST_LATITUDE:      u16 = 0x0014;
pub const EXIF_TAG_GPS_DEST_LONGITUDE_REF: u16 = 0x0015;
pub const EXIF_TAG_GPS_DEST_LONGITUDE:     u16 = 0x0016;
pub const EXIF_TAG_GPS_DEST_BEARING_REF:   u16 = 0x0017;
pub const EXIF_TAG_GPS_DEST_BEARING:       u16 = 0x0018;
pub const EXIF_TAG_GPS_DEST_DISTANCE_REF:  u16 = 0x0019;
pub const EXIF_TAG_GPS_DEST_DISTANCE:      u16 = 0x001A;
pub const EXIF_TAG_GPS_PROCESSING_METHOD:  u16 = 0x001B;
pub const EXIF_TAG_GPS_AREA_INFORMATION:   u16 = 0x001C;
pub const EXIF_TAG_GPS_DATE_STAMP:         u16 = 0x001D;
pub const EXIF_TAG_GPS_DIFFERENTIAL:       u16 = 0x001E;

#[derive(Debug,PartialEq,Eq,Clone,Copy,Hash)]
#[repr(C)]
pub enum ExifTag {
    EXIF_TAG_INTEROPERABILITY_INDEX         = 0x0001,
    EXIF_TAG_INTEROPERABILITY_VERSION       = 0x0002,
    EXIF_TAG_NEW_SUBFILE_TYPE               = 0x00FE,
    EXIF_TAG_IMAGE_WIDTH                    = 0x0100,
    EXIF_TAG_IMAGE_LENGTH                   = 0x0101,
    EXIF_TAG_BITS_PER_SAMPLE                = 0x0102,
    EXIF_TAG_COMPRESSION                    = 0x0103,
    EXIF_TAG_PHOTOMETRIC_INTERPRETATION     = 0x0106,
    EXIF_TAG_FILL_ORDER                     = 0x010A,
    EXIF_TAG_DOCUMENT_NAME                  = 0x010D,
    EXIF_TAG_IMAGE_DESCRIPTION              = 0x010E,
    EXIF_TAG_MAKE                           = 0x010F,
    EXIF_TAG_MODEL                          = 0x0110,
    EXIF_TAG_STRIP_OFFSETS                  = 0x0111,
    EXIF_TAG_ORIENTATION                    = 0x0112,
    EXIF_TAG_SAMPLES_PER_PIXEL              = 0x0115,
    EXIF_TAG_ROWS_PER_STRIP                 = 0x0116,
    EXIF_TAG_STRIP_BYTE_COUNTS              = 0x0117,
    EXIF_TAG_X_RESOLUTION                   = 0x011A,
    EXIF_TAG_Y_RESOLUTION                   = 0x011B,
    EXIF_TAG_PLANAR_CONFIGURATION           = 0x011C,
    EXIF_TAG_RESOLUTION_UNIT                = 0x0128,
    EXIF_TAG_TRANSFER_FUNCTION              = 0x012D,
    EXIF_TAG_SOFTWARE                       = 0x0131,
    EXIF_TAG_DATE_TIME                      = 0x0132,
    EXIF_TAG_ARTIST                         = 0x013B,
    EXIF_TAG_WHITE_POINT                    = 0x013E,
    EXIF_TAG_PRIMARY_CHROMATICITIES         = 0x013F,
    EXIF_TAG_SUB_IFDS                       = 0x014A,
    EXIF_TAG_TRANSFER_RANGE                 = 0x0156,
    EXIF_TAG_JPEG_PROC                      = 0x0200,
    EXIF_TAG_JPEG_INTERCHANGE_FORMAT        = 0x0201,
    EXIF_TAG_JPEG_INTERCHANGE_FORMAT_LENGTH = 0x0202,
    EXIF_TAG_YCBCR_COEFFICIENTS             = 0x0211,
    EXIF_TAG_YCBCR_SUB_SAMPLING             = 0x0212,
    EXIF_TAG_YCBCR_POSITIONING              = 0x0213,
    EXIF_TAG_REFERENCE_BLACK_WHITE          = 0x0214,
    EXIF_TAG_XML_PACKET                     = 0x02BC,
    EXIF_TAG_RELATED_IMAGE_FILE_FORMAT      = 0x1000,
    EXIF_TAG_RELATED_IMAGE_WIDTH            = 0x1001,
    EXIF_TAG_RELATED_IMAGE_LENGTH           = 0x1002,
    EXIF_TAG_CFA_REPEAT_PATTERN_DIM         = 0x828D,
    EXIF_TAG_CFA_PATTERN                    = 0x828E,
    EXIF_TAG_BATTERY_LEVEL                  = 0x828F,
    EXIF_TAG_COPYRIGHT                      = 0x8298,
    EXIF_TAG_EXPOSURE_TIME                  = 0x829A,
    EXIF_TAG_FNUMBER                        = 0x829D,
    EXIF_TAG_IPTC_NAA                       = 0x83BB,
    EXIF_TAG_IMAGE_RESOURCES                = 0x8649,
    EXIF_TAG_EXIF_IFD_POINTER               = 0x8769,
    EXIF_TAG_INTER_COLOR_PROFILE            = 0x8773,
    EXIF_TAG_EXPOSURE_PROGRAM               = 0x8822,
    EXIF_TAG_SPECTRAL_SENSITIVITY           = 0x8824,
    EXIF_TAG_GPS_INFO_IFD_POINTER           = 0x8825,
    EXIF_TAG_ISO_SPEED_RATINGS              = 0x8827,
    EXIF_TAG_OECF                           = 0x8828,
    EXIF_TAG_TIME_ZONE_OFFSET               = 0x882A,
    EXIF_TAG_EXIF_VERSION                   = 0x9000,
    EXIF_TAG_DATE_TIME_ORIGINAL             = 0x9003,
    EXIF_TAG_DATE_TIME_DIGITIZED            = 0x9004,
    EXIF_TAG_COMPONENTS_CONFIGURATION       = 0x9101,
    EXIF_TAG_COMPRESSED_BITS_PER_PIXEL      = 0x9102,
    EXIF_TAG_SHUTTER_SPEED_VALUE            = 0x9201,
    EXIF_TAG_APERTURE_VALUE                 = 0x9202,
    EXIF_TAG_BRIGHTNESS_VALUE               = 0x9203,
    EXIF_TAG_EXPOSURE_BIAS_VALUE            = 0x9204,
    EXIF_TAG_MAX_APERTURE_VALUE             = 0x9205,
    EXIF_TAG_SUBJECT_DISTANCE               = 0x9206,
    EXIF_TAG_METERING_MODE                  = 0x9207,
    EXIF_TAG_LIGHT_SOURCE                   = 0x9208,
    EXIF_TAG_FLASH                          = 0x9209,
    EXIF_TAG_FOCAL_LENGTH                   = 0x920A,
    EXIF_TAG_SUBJECT_AREA                   = 0x9214,
    EXIF_TAG_TIFF_EP_STANDARD_ID            = 0x9216,
    EXIF_TAG_MAKER_NOTE                     = 0x927C,
    EXIF_TAG_USER_COMMENT                   = 0x9286,
    EXIF_TAG_SUB_SEC_TIME                   = 0x9290,
    EXIF_TAG_SUB_SEC_TIME_ORIGINAL          = 0x9291,
    EXIF_TAG_SUB_SEC_TIME_DIGITIZED         = 0x9292,
    EXIF_TAG_XP_TITLE                       = 0x9C9B,
    EXIF_TAG_XP_COMMENT                     = 0x9C9C,
    EXIF_TAG_XP_AUTHOR                      = 0x9C9D,
    EXIF_TAG_XP_KEYWORDS                    = 0x9C9E,
    EXIF_TAG_XP_SUBJECT                     = 0x9C9F,
    EXIF_TAG_FLASH_PIX_VERSION              = 0xA000,
    EXIF_TAG_COLOR_SPACE                    = 0xA001,
    EXIF_TAG_PIXEL_X_DIMENSION              = 0xA002,
    EXIF_TAG_PIXEL_Y_DIMENSION              = 0xA003,
    EXIF_TAG_RELATED_SOUND_FILE             = 0xA004,
    EXIF_TAG_INTEROPERABILITY_IFD_POINTER   = 0xA005,
    EXIF_TAG_FLASH_ENERGY                   = 0xA20B,
    EXIF_TAG_SPATIAL_FREQUENCY_RESPONSE     = 0xA20C,
    EXIF_TAG_FOCAL_PLANE_X_RESOLUTION       = 0xA20E,
    EXIF_TAG_FOCAL_PLANE_Y_RESOLUTION       = 0xA20F,
    EXIF_TAG_FOCAL_PLANE_RESOLUTION_UNIT    = 0xA210,
    EXIF_TAG_SUBJECT_LOCATION               = 0xA214,
    EXIF_TAG_EXPOSURE_INDEX                 = 0xA215,
    EXIF_TAG_SENSING_METHOD                 = 0xA217,
    EXIF_TAG_FILE_SOURCE                    = 0xA300,
    EXIF_TAG_SCENE_TYPE                     = 0xA301,
    EXIF_TAG_NEW_CFA_PATTERN                = 0xA302,
    EXIF_TAG_CUSTOM_RENDERED                = 0xA401,
    EXIF_TAG_EXPOSURE_MODE                  = 0xA402,
    EXIF_TAG_WHITE_BALANCE                  = 0xA403,
    EXIF_TAG_DIGITAL_ZOOM_RATIO             = 0xA404,
    EXIF_TAG_FOCAL_LENGTH_IN_35MM_FILM      = 0xA405,
    EXIF_TAG_SCENE_CAPTURE_TYPE             = 0xA406,
    EXIF_TAG_GAIN_CONTROL                   = 0xA407,
    EXIF_TAG_CONTRAST                       = 0xA408,
    EXIF_TAG_SATURATION                     = 0xA409,
    EXIF_TAG_SHARPNESS                      = 0xA40A,
    EXIF_TAG_DEVICE_SETTING_DESCRIPTION     = 0xA40B,
    EXIF_TAG_SUBJECT_DISTANCE_RANGE         = 0xA40C,
    EXIF_TAG_IMAGE_UNIQUE_ID                = 0xA420,
    EXIF_TAG_GAMMA                          = 0xA500,
    EXIF_TAG_PRINT_IMAGE_MATCHING           = 0xC4A5,
    EXIF_TAG_PADDING                        = 0xEA1C,
}

#[derive(Debug,PartialEq,Eq,Clone,Copy,Hash)]
#[repr(C)]
pub enum ExifSupportLevel {
    EXIF_SUPPORT_LEVEL_UNKNOWN = 0,
    EXIF_SUPPORT_LEVEL_NOT_RECORDED,
    EXIF_SUPPORT_LEVEL_MANDATORY,
    EXIF_SUPPORT_LEVEL_OPTIONAL,
}

extern "C" {
    pub fn exif_tag_from_name(name: *const c_char) -> ExifTag;
    pub fn exif_tag_get_name_in_ifd(tag: ExifTag, ifd: ExifIfd) -> *const c_char;
    pub fn exif_tag_get_title_in_ifd(tag: ExifTag, ifd: ExifIfd) -> *const c_char;
    pub fn exif_tag_get_description_in_ifd(tag: ExifTag, ifd: ExifIfd) -> *const c_char;
    pub fn exif_tag_get_support_level_in_ifd(tag: ExifTag, ifd: ExifIfd, t: ExifDataType) -> ExifSupportLevel;
    pub fn exif_tag_get_name(tag: ExifTag) -> *const c_char;
    pub fn exif_tag_get_title(tag: ExifTag) -> *const c_char;
    pub fn exif_tag_get_description(tag: ExifTag) -> *const c_char;

    pub fn exif_tag_table_get_tag(n: c_uint) -> ExifTag;
    pub fn exif_tag_table_get_name(n: c_uint) -> *const c_char;
    pub fn exif_tag_table_count() -> c_uint;
}
