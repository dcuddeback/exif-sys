use libc::{c_void, c_uint};

use data::ExifData;
use entry::ExifEntry;
use ifd::ExifIfd;
use mem::ExifMem;
use tag::ExifTag;

#[repr(C)]
pub struct ExifContent {
    pub entries: *mut *mut ExifEntry,
    pub count: c_uint,
    pub parent: *mut ExifData,
    private: *mut ExifContentPrivate,
}

#[repr(C)]
pub struct ExifContentPrivate {
    __private: c_void,
}

pub type ExifContentForeachEntryFunc = extern "C" fn (entry: *mut ExifEntry, data: *mut c_void);

extern "C" {
    pub fn exif_content_new() -> *mut ExifContent;
    pub fn exif_content_new_mem(mem: *mut ExifMem) -> *mut ExifContent;
    pub fn exif_content_ref(content: *mut ExifContent);
    pub fn exif_content_unref(content: *mut ExifContent);
    pub fn exif_content_free(content: *mut ExifContent);
    pub fn exif_content_add_entry(content: *mut ExifContent, entry: *mut ExifEntry);
    pub fn exif_content_remove_entry(content: *mut ExifContent, entry: *mut ExifEntry);
    pub fn exif_content_get_entry(content: *mut ExifContent, tag: ExifTag) -> *mut ExifEntry;
    pub fn exif_content_fix(content: *mut ExifContent);
    pub fn exif_content_foreach_entry(content: *mut ExifContent, func: ExifContentForeachEntryFunc, data: *mut c_void);
    pub fn exif_content_get_ifd(content: *mut ExifContent) -> ExifIfd;
    pub fn exif_content_dump(content: *mut ExifContent, indent: c_uint);
}
