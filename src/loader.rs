use libc::{c_void, c_char, c_uchar, c_uint};

use data::ExifData;
use mem::ExifMem;

#[repr(C)]
pub struct ExifLoader {
    __private: c_void,
}

extern "C" {
    pub fn exif_loader_new() -> *mut ExifLoader;
    pub fn exif_loader_new_mem(mem: *mut ExifMem) -> *mut ExifLoader;
    pub fn exif_loader_ref(loader: *mut ExifLoader);
    pub fn exif_loader_unref(loader: *mut ExifLoader);
    pub fn exif_loader_write_file(loader: *mut ExifLoader, file_name: *const c_char);
    pub fn exif_loader_write(loader: *mut ExifLoader, buf: *const c_uchar, size: c_uint) -> c_uchar;
    pub fn exif_loader_reset(loader: *mut ExifLoader);
    pub fn exif_loader_get_data(loader: *mut ExifLoader) -> *mut ExifData;
    pub fn exif_loader_get_buf(loader: *mut ExifLoader, buf: *mut *const c_uchar, buf_size: *mut c_uint);
}
