use libc::{c_char, c_uchar};

pub use ExifFormat::*;

#[derive(Debug,PartialEq,Eq,Clone,Copy,Hash)]
#[repr(C)]
pub enum ExifFormat {
    EXIF_FORMAT_BYTE = 1,
    EXIF_FORMAT_ASCII = 2,
    EXIF_FORMAT_SHORT = 3,
    EXIF_FORMAT_LONG = 4,
    EXIF_FORMAT_RATIONAL = 5,
    EXIF_FORMAT_SBYTE = 6,
    EXIF_FORMAT_UNDEFINED = 7,
    EXIF_FORMAT_SSHORT = 8,
    EXIF_FORMAT_SLONG = 9,
    EXIF_FORMAT_SRATIONAL = 10,
    EXIF_FORMAT_FLOAT = 11,
    EXIF_FORMAT_DOUBLE = 12,
}

extern "C" {
    pub fn exif_format_get_name(format: ExifFormat) -> *const c_char;
    pub fn exif_format_get_size(format: ExifFormat) -> c_uchar;
}
