pub use ExifByteOrder::*;

#[repr(C)]
pub enum ExifByteOrder {
    EXIF_BYTE_ORDER_MOTOROLA,
    EXIF_BYTE_ORDER_INTEL,
}
